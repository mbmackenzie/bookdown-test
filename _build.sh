#!/bin/sh

set -ev

Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook', output_dir = 'public')"
Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::pdf_book', output_dir = 'public')"
Rscript -e "file.rename('public/Test-Book.pdf', 'public/Test Book.pdf')"
